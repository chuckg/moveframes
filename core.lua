moveFrames = AceLibrary("AceAddon-2.0"):new("AceConsole-2.0","AceDB-2.0","AceDebug-2.0","AceEvent-2.0","AceHook-2.1");
moveFrames:RegisterDB("moveFramesDBPC");
moveFrames:RegisterDefaults("char", {
	globalLock = false,
} )

local moveFrames_AddonFrames = {}

local ENABLE_STRING, DISABLE_STRING
ENABLE_STRING = "|cff00ff00Enabled|r"
DISABLE_STRING = "|cffff0000Disabled|r"

local options = { 
    type='group',
	args = {
		reset = {
			type = "execute",
			name = "Reset",
			desc = "Reset all the frame positions",
			func = function()
				moveFrames:Reset()
			end,
		},
		lock = {
			type = "toggle",
			name = "Lock",
			desc = "Lock all frames into position.",
			get = function()
				return moveFrames.db.char.globalLock
			end,
			set = function(v)
				moveFrames.db.char.globalLock = v
			end,
			map = { [false] = DISABLE_STRING, [true] = ENABLE_STRING },
		}
	}
}

function moveFrames:OnInitialize()
	self:RegisterChatCommand({ "/mf", "/moveframes" }, options);
	
	-- Blizzard Addon definition list.
	moveFrames_AddonFrames = {
		-- Format:
		-- Blizzard_Addon 		= { "FrameName", "moveFrameName" }, 
		-- Blizzard_BindingUI	= { "KeyBindingFrame", "KeyBindingFrameMF" },
		["Blizzard_AuctionUI"]		= "AuctionFrame", 
		["Blizzard_CraftUI"]		= "CraftFrame",
		["Blizzard_InspectUI"]		= "InspectFrame",
		["Blizzard_MacroUI"]		= "MacroFrame",
		["Blizzard_TalentUI"]		= "TalentFrame",
		["Blizzard_TradeSkillUI"]	= "TradeSkillFrame",
		["Blizzard_TrainerUI"]		= "ClassTrainerFrame",
	}
end

function moveFrames:OnEnable()
	self:SetDebugging(false)
	self:Debug("moveFrames enabled.")
	local frames = moveFrames_AddonFrames
	local captureBar = getglobal(ExtendedUI["CAPTUREPOINT"].name .. "1") or nil
	
	-- Check if, for some reason, any of the LOD stuff is loaded.  If it is, we'll initialize the moveFrame stuff for them.
	for addonName, frameName in pairs(frames) do
		if IsAddOnLoaded(addonName) then
			self:CreateMoveFrame(frameName)
		end
	end
	
	-- Check if the captureBar has been created already, if it has, we'll do our createFrames.
	if captureBar then
		self:ScheduleEvent(function() self:CreateMoveFrame(captureBar:GetName(), "moveFramesFloatTemplate") end, .25)
		self:ManageFramePositions()
	end
	
	-- Hook the addonLoaded event to catch any addons which weren't loaded so we can init the moveFrame junk.
	self:RegisterEvent("ADDON_LOADED")
	-- Hook the SetCenterFrame and SetLeftFrame so we can throw skipSetPoint.
	self:Hook("MovePanelToCenter", "MovePanelToCenter", true)
	self:Hook("MovePanelToLeft", "MovePanelToLeft", true)
	self:Hook("QuestWatch_Update", true)
	self:Hook(ExtendedUI.CAPTUREPOINT, "create", "CaptureBar_Create", true)
	self:Hook("UIParent_ManageFramePositions", "ManageFramePositions", true)
end

function moveFrames:OnDisable()
    -- Called when the addon is disabled
end

function moveFrames:Reset(f, delay, showMessage)
	if f then
		local pframe = f:GetParent()
		-- Hide the frame first.
		HideUIPanel(pframe)
		-- Clear any of it's positional settings
		self.db.char[pframe:GetName()] = nil
		pframe:ClearAllPoints()
		
		-- Special cases can't figure out another way around them.
		if pframe:GetName() == "WorldStateAlwaysUpFrame" then
			pframe:SetPoint("TOP", 5, -15)
		elseif pframe:GetName() == "CastingBarFrame" then
			pframe:SetUserPlaced(false)
		elseif string.find(pframe:GetName(), "^WorldStateCaptureBar%d$") then
			delay = -1
		end
		-- Force the UIParent_ManageFramePositions to reposition basic elements.
		self:ManageFramePositions()
		
		-- If we want to delay when it's reshown, do it here.
		if delay > -1 then 
			self:ScheduleEvent(function() ShowUIPanel(pframe) end, delay)
		end
		
		if showMessage then self:Print(pframe:GetName() .. " has been reset.") end
	else
		temp = self.db.char.globalLock 
		self:ResetDB("moveFramesDBPC", "char")
		self.db.char.globalLock = temp
		-- Force the UIParent_ManageFramePositions to reposition basic elements.
		self:ManageFramePositions()
	end
end

function moveFrames:CreateMoveFrame(parentFrame, template)
	local mFrame = parentFrame .."MF"
	
	-- "Just in case code" we've already created the move frame, just end.
	if getglobal(mFrame) and getglobal(mFrame):GetName() ~= nil then 
		return 
	end
	
	-- Create the moveFrame.
	local f = CreateFrame("Frame", mFrame, getglobal(parentFrame), template or "moveFramesTemplate")
	f:SetID(34)
end

function moveFrames:IsLocked(f)
	local pframe, frameInfo
	
	pframe = f:GetParent()
	frameInfo = self.db.char[pframe:GetName()] or nil
	
	if frameInfo and frameInfo.locked or self.db.char.globalLock == true then
		return true
	else
		return false
	end
end

function moveFrames:ToggleLock(f, showMessage)
	local pframe, frameInfo
	
	pframe = f:GetParent():GetName()
	if not self.db.char[pframe] then
		self.db.char[pframe] = {}
	end
	frameInfo = self.db.char[pframe]
	
	if frameInfo.locked then
		frameInfo.locked = false
		if showMessage then self:Print(pframe .." has been [".. ENABLE_STRING .."].") end
	else
		frameInfo.locked = true
		if showMessage then self:Print(pframe .." has been [".. DISABLE_STRING .."].") end
	end
end

--<< ====================================================================== >>--
-- Events / Hooks		                                                      --
--<< ====================================================================== >>--
function moveFrames:ADDON_LOADED()
	local frame = moveFrames_AddonFrames[arg1]
	
	if frame ~= nil then
		self:CreateMoveFrame(frame)
	end
end

function moveFrames:MovePanelToCenter()
	if UIParent.left then
		local frameInfo = self.db.char[UIParent.left:GetName()]
		SetCenterFrame(nil, true)
		if not frameInfo or frameInfo and not frameInfo.x then
			UIParent.left:SetPoint("TOPLEFT", "UIParent", "TOPLEFT", 384, -104)
			UIParent.left:Raise()
		end
		UIParent.center = UIParent.left
		UIParent.left = nil
	end
end

function moveFrames:MovePanelToLeft()
	if UIParent.center then
		local frameInfo = self.db.char[UIParent.center:GetName()]
		SetLeftFrame(nil, true)
		if not frameInfo or frameInfo and not frameInfo.x then
			UIParent.center:SetPoint("TOPLEFT", "UIParent", "TOPLEFT", 0, -104)
		end
		UIParent.left = UIParent.center
		UIParent.center = nil;
	end
end

function moveFrames:QuestWatch_Update()
	self.hooks["QuestWatch_Update"]()
	local frame = "QuestWatchFrameMF"
	local pframe = getglobal(frame):GetParent()
	getglobal(frame):SetHeight(pframe:GetHeight())
	getglobal(frame):SetWidth(pframe:GetWidth())
end

function moveFrames:CaptureBar_Create( id ) 
	self:Debug("CaptureBar_Create( ".. id .." )")  
	local captureBar = self.hooks[ExtendedUI.CAPTUREPOINT].create(id)
	if captureBar then
		self:ScheduleEvent(function() self:CreateMoveFrame(captureBar:GetName(), "moveFramesFloatTemplate") end, .25)
		self:ManageFramePositions()
	end
	return captureBar
end

function moveFrames:ManageFramePositions()
	local f = "WorldStateCaptureBar1"
	local frame = self.db.char[f]
	
	if getglobal(f) and getglobal(f).isMoving then return end
	
	if self.hooks and self.hooks["UIParent_ManageFramePositions"] then self.hooks["UIParent_ManageFramePositions"]() end
	if NUM_EXTENDED_UI_FRAMES and ( frame and frame.x ) then
		local captureBar
		local numCaptureBars = 0
		for i=1, NUM_EXTENDED_UI_FRAMES do
			captureBar = getglobal("WorldStateCaptureBar"..i)
			if ( captureBar and captureBar:IsShown() ) then
				captureBar:ClearAllPoints()
				captureBar:SetPoint("TOPLEFT", captureBar:GetParent(), frame.x, frame.y)
			end
		end	
	end
end

--<< ====================================================================== >>--
-- SetScript Events                                                           --
--<< ====================================================================== >>--
function moveFrames:onLoad(f)
	moveFrames:OverlayOnLoad(f)
	
	--set to parent width
	local pframe = f:GetParent()
	if pframe then
		f:SetWidth(pframe:GetWidth())
	end
	
	--subtract HitRectInsets/AbsInset/right value of parent stored in bar id
	--only needed since there's no GetRightHitRectInset()
	local widthOffeset = f:GetID();
	if widthOffeset then
		f:SetWidth(f:GetWidth()-widthOffeset)
	end
end

function moveFrames:onShow(f)
	if f then
		local pframe = f:GetParent()
		if pframe then
			local pframeName = pframe:GetName()
			if self.db.char and self.db.char[pframeName] then
				local frameInfo = self.db.char[pframeName]
				if frameInfo.x then
					pframe:SetPoint("TOPLEFT",UIParent,"TOPLEFT",frameInfo.x ,frameInfo.y )
				end
			end
		end
	end
end

function moveFrames:onMouseDown(f)
	if not f then
		return
	end
	
	if IsMouseButtonDown(2) and IsShiftKeyDown() then
		self:Reset(f, .25, true)
		self.rightClick = true
		return
	elseif IsMouseButtonDown(2) then
		self:ToggleLock(f, true)
		self.rightClick = true
		return
	end
	
	self:Debug("Frame ".. f:GetName() .. " clicked.")
	
	if f and not self:IsLocked(f) then
		self:barStartDrag(f)
	end
end

function moveFrames:onMouseUp(f)
	if f and not self:IsLocked(f) and not self.rightClick then
		self:barStopDrag(f)
	else
		self.rightClick = false
	end
end

--<< ====================================================================== >>--
-- SetScript Utility Functions                                                --
--<< ====================================================================== >>--
function moveFrames:OverlayOnLoad(f)
	frame = f:GetParent()
	if frame then
		if not frame:IsMovable() then
			frame:SetMovable(1)
		end
	end
end

function moveFrames:onDragStart(f)
	if f then
		if f.isMoving then
			f:StopMovingOrSizing()
			f.isMoving = false
		end
	end
end

function moveFrames:startDrag(f)
	if f then
		if ( not f.isLocked or f.isLocked == 0 ) and arg1 == "LeftButton" then
			f:StartMoving()
			f.isMoving = true
			f.isReset = nil
		end
	end
end

function moveFrames:stopDrag(f)
	if f then
		if not self.db.char[f:GetName()] then
			self.db.char[f:GetName()] = {}
		end
		local frameInfo = self.db.char[f:GetName()]
		
		if (f.isMoving) then
			f:StopMovingOrSizing()
			f.isMoving = false
		end
		
		local point, relativeTo, relativePoint, xoff, yoff = f:GetPoint()
		frameInfo.x = xoff
		frameInfo.y = yoff
	end
end

function moveFrames:barOnDragStart(f)
	if f then
		self:onDragStart(f:GetParent())
	end
end

function moveFrames:barStartDrag(f)
	if f then
		self:startDrag(f:GetParent())
	end
end

function moveFrames:barStopDrag(f)
	if f then
		self:stopDrag(f:GetParent())
	end
end