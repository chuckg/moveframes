moveFrames 1.0

Author: chuckg (c@chuckg.org)
Realm: Blindchuck <Ropetown>, Executus
Website: http://chuckg.org, http://chuckg.wowinterface.com
Date: 2007-03-13 18:58:12

moveFrames does exactly what it's name implies, it allows you to move the basic
Blizzard frames anywhere you would like.  No more frames locked to the left and
center of the screen. Think of it as a lightweight version of MobileFrames, 
MoveIt, or DiscordFrameModifier only without all the extra stuff.  It doesn't
do anymore than it's name implies.  moveFrames is very unobtrusive, there are
no visible frames or buttons cluttering up the UI or special slash commands used 
to move a frame.

To move a frame, simply drag by the 'title bar' to the location you want the 
frame placed. The next time you re-open the frame, it will open in it's newly 
placed position and no longer adhere to the "frame push" behavior when opened 
or closed like the default. Frames that have not been moved using moveFrames 
retain the default blizzard behavior, opening on the left and pushing currently 
open ones to the middle.

moveFrames works with just about every frame, including the LoadOnDemand frames like
TradeSkillFrame or TalentUI.  Currently, every frame is enabled by default
but you may turn off / lock individual frames; check the 'Usage' section for more
details.  

Here's a list of frames that are currently movable:
Blizzard_AuctionUI
Blizzard_CraftUI
Blizzard_InspectUI
Blizzard_MacroUI
Blizzard_TalentUI
Blizzard_TradeSkillUI
Blizzard_TrainerUI
QuestLogFrame
TradeFrame
MerchantFrame
ClassTrainerFrame
PetStableFrame
BattlefieldFrame
FriendsFrame
CharacterFrame
SpellBookFrame
BankFrame
QuestFrame
GossipFrame
GuildRegistrarFrame
ItemTextFrame
TabardFrame
MailFrame
OpenMailFrame
WorldStateScoreFrame
DressUpFrame
TaxiFrame
LootFrame
QuestWatchFrame
WorldStateCaptureBar(1-9)
WorldStateAlwaysUpFrame
CastingBarFrame


moveFrames was not originally written by me, I have just picked up the project
and extended it's use to fit all frames within the default Blizzard framework
as well as made some other changes that seemed appropriate in keeping with
the purpose of this mod. It was originally written by Stabler and the original
may be found at the following URL: http://wowinterface.com/downloads/fileinfo.php?id=6564

Usage:
The slash commands offer a number of options and may be accessed in one of two ways: /moveframes or /mf
/mf reset 	  - This will reset all of the moveFrames positional data. The next time 
                    frames are opened, they will use the default Blizzard positioning.
/mf lock 	  - This toggle a global lock on all moveFrames, frames will not be moveable with this toggled.

There are also a number of ways to handle individual frame settings by click on the 
TitleBar area or, in the case of frames like QuestWatch and WorldStateCaptureBar, 
on the frame itself.
Left-Click+Drag   - Move a frame wherever you want.  Pretty straight forward.
Right-Click 	  - This will lock the selected frame into position.  To un-lock the frame, 
		    just right-click it again.
Shift+Right-Click - This will reset the moveFrame data. Do this if there is one 
		    frame that needs to be reset without losing every frames positional data.


Leave a message for me in the comments or at WoWInterface bug report if you 
have any issues / ideas / updates / etc. to share with me.

TO INSTALL: Put the moveFrames folder into
	\World of Warcraft\Interface\AddOns\

This mod runs using the embedded Ace2 library.  What that means is that you do not
have to install any third hand libraries in order to get moveFrames to work; it just
will.  If you have other Ace2 mods installed, they should play nice.