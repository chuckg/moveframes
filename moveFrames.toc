## Interface: 20003
## Title: moveFrames|cff7fff7f -Ace2-|r
## Notes: Move any default Blizzard UI frame.
## Version: 1.0
## Author: chuckg (Blindchuck <Ropetown>)
## X-eMail: c@chuckg.org
## X-Website: http://chuckg.wowinterface.org
## X-Category: Interface Enhancements
## X-Date: 2007-03-13
## X-Embeds: Ace2
## SavedVariablesPerCharacter: moveFramesDBPC
## OptionalDeps: Ace2
## Dependencies: 


libs\AceLibrary\AceLibrary.lua
libs\AceOO-2.0\AceOO-2.0.lua
libs\AceEvent-2.0\AceEvent-2.0.lua
libs\AceHook-2.1\AceHook-2.1.lua
libs\AceDB-2.0\AceDB-2.0.lua
libs\AceDebug-2.0\AceDebug-2.0.lua
libs\AceConsole-2.0\AceConsole-2.0.lua
libs\AceAddon-2.0\AceAddon-2.0.lua

core.lua
core.xml
